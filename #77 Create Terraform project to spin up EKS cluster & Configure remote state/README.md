Creating multiple Amazon EKS clusters with Terraform is an efficient way to set up identical environments for development, testing, and staging. This guide outlines how to create a Terraform project for spinning up EKS clusters, configure remote state storage, and automate the process using a Jenkins pipeline.

## Exercise 1: Create Terraform Project for EKS Cluster
Step 1: Initialize Your Terraform Project

Create a new directory for your Terraform project and initialize a Git repository.

```bash
mkdir eks-terraform
cd eks-terraform
git init
```

Create the main Terraform configuration files: main.tf, variables.tf, outputs.tf, and provider.tf.
## Step 2: Define the AWS Provider and EKS Cluster

In provider.tf, specify the AWS provider and the required version.

```bash
provider "aws" {
  region = "eu-central-1"
}
```

Use the terraform-aws-modules/eks/aws module in main.tf to create the EKS cluster. Specify the cluster name, number of nodes, and the Fargate profile for the "my-app" namespace.

```bash
module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = "my-cluster"
  cluster_version = "1.28"
  subnets         = module.vpc.private_subnets

  node_groups = {
    eks_nodes = {
      desired_capacity = 3
      max_capacity     = 3
      min_capacity     = 1
    }
  }

  fargate_profiles = {
    my_app = {
      namespace = "my-app"
    }
  }
}
```

Ensure you have the VPC module configured in your Terraform to provision the required networking resources for EKS.

## Step 3: Deploy MySQL Using Helm Provider
Integrate the Helm provider in your Terraform to deploy MySQL from the Bitnami chart. Define the Helm provider and release resource in main.tf.

```bash
provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

resource "helm_release" "mysql" {
  name       = "my-release"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mysql"
  version    = "x.y.z" # Specify the chart version

  set {
    name  = "architecture"
    value = "replication"
  }

  set {
    name  = "replicaCount"
    value = "3"
  }

  # Include additional configurations as needed
}
```

## Step 4: Create the Git Repository
Host your Terraform project in a new Git repository separate from the Java application codebase.

# Exercise 2: Configure Remote State with S3
In your Terraform project, configure the backend to use Amazon S3 for state management.

```bash
terraform {
  backend "s3" {
    bucket = "my-terraform-state-bucket"
    key    = "state/eks-cluster"
    region = "eu-central-1"
  }
}
```

Make sure to create the S3 bucket beforehand or include it in your Terraform configuration.

## Jenkins Pipeline for Terraform Project
To automate the provisioning process, create a Jenkinsfile in your Terraform project repository. This pipeline should include stages for initializing Terraform, applying changes, and optionally, a stage for destroying environments.

```bash
pipeline {
  agent any
  stages {
    stage('Init') {
      steps {
        sh 'terraform init'
      }
    }
    stage('Apply') {
      steps {
        sh 'terraform apply -auto-approve -var-file="dev.tfvars"'
      }
    }
    // Include additional stages as necessary
  }
}
```

Ensure Jenkins has the necessary plugins and permissions to interact with AWS and run Terraform commands.

## Accessing the Cluster
After Terraform successfully provisions the EKS clusters, configure kubectl to interact with your new cluster:

```bash
aws eks update-kubeconfig --name my-cluster --region eu-central-1
```

Verify the cluster and nodes:

```bash
kubectl get nodes
```

This approach allows you to efficiently manage multiple EKS environments with identical configurations, facilitating a consistent and automated workflow across development, testing, and staging phases.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/infrastructure-as-code-with-terraform/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
