Creating a CI/CD pipeline for your Terraform project that provisions an Amazon EKS cluster is an effective way to automate infrastructure management. The pipeline will enable you to apply infrastructure changes through source control management, ensuring consistency and traceability. Below is a simplified example of how you can set up such a Jenkins pipeline, including setting environment variables and using Jenkins credentials for AWS access.

## Jenkins Pipeline Setup
Define Jenkins Credentials:

First, you need to store your AWS credentials securely in Jenkins:

Navigate to Jenkins Dashboard > Manage Jenkins > Manage Credentials.
Under the (global) domain, click Add Credentials.
Choose AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY as Secret text type credentials. Give them respective IDs that you'll reference in the Jenkinsfile (e.g., awsAccessKeyId and awsSecretAccessKey).
Jenkinsfile for Terraform CI/CD Pipeline:

Create a Jenkinsfile in your Terraform project repository with the following content:

```bash
pipeline {
    agent any
    environment {
        // Define environment variables
        TF_VAR_env_prefix = "dev"
        TF_VAR_k8s_version = "1.28"
        TF_VAR_cluster_name = "my-cluster"
        TF_VAR_region = "eu-central-1"
        // Reference Jenkins credentials
        AWS_ACCESS_KEY_ID = credentials('awsAccessKeyId')
        AWS_SECRET_ACCESS_KEY = credentials('awsSecretAccessKey')
    }
    stages {
        stage('Initialize') {
            steps {
                sh 'terraform init'
            }
        }
        stage('Validate') {
            steps {
                sh 'terraform validate'
            }
        }
        stage('Plan') {
            steps {
                sh 'terraform plan'
            }
        }
        stage('Apply') {
            steps {
                input message: 'Are you sure you want to apply this Terraform plan?'
                sh 'terraform apply -auto-approve'
            }
        }
    }
    post {
        always {
            // Cleanup, notifications or other steps
        }
    }
}
```

This Jenkinsfile defines a pipeline with stages for initializing Terraform, validating the configuration, planning the changes, and applying them. The input step in the Apply stage requires manual approval, giving you a chance to review changes before they're applied. Environment variables specific to Terraform are prefixed with TF_VAR_, which Terraform automatically recognizes.

## Running the Pipeline
- Commit the Jenkinsfile to your repository and set up a new Jenkins pipeline job pointing to it.
- When the pipeline runs, it uses the provided AWS credentials to authenticate with AWS and apply the Terraform configurations.
- The pipeline parameters allow you to dynamically adjust Terraform variables for different environments or configurations.


## Security Considerations
- Ensure that the AWS credentials provided to Jenkins have the minimum required permissions for provisioning the EKS cluster and associated resources. Adhering to the principle of least privilege reduces the risk of unauthorized access or actions.
- Consider implementing additional security measures in Jenkins, such as role-based access control (RBAC), to limit who can trigger this pipeline and apply changes to your infrastructure.

By following these steps, you create an automated CI/CD pipeline for managing your EKS clusters with Terraform through Jenkins, enhancing your team's ability to deploy and manage Kubernetes infrastructure efficiently and securely.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/infrastructure-as-code-with-terraform/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
